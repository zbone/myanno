package com.zbystudy.factory;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Created By zby on 20:35 2019/4/1
 * 创建实体类的工厂
 */

public interface EntityFactory {

    /**
     * Created By zby on 15:01 2019/4/1
     *
     * @param poPackage      po包名
     * @param fromConfigFile 是否来源于配置文件
     */
    Set<File> loadEntityFiles(String poPackage, boolean fromConfigFile);

    /**
     * Created By zby on 15:02 2019/4/1
     *
     * @param poPackage      po包名
     * @param fromConfigFile 是否来源于配置文件
     */
    List<String> loadEntityFileNames(String poPackage, boolean fromConfigFile);

    /**
     * Created By zby on 14:59 2019/4/1
     * 解析文件路径
     *
     * @param poPackage      包名
     * @param fromConfigFile 是否来源于配置文件
     */
    String parsePackagePath(String poPackage, boolean fromConfigFile);

    /**
     * Created By zby on 16:13 2019/4/1
     *
     * @param poPackage      包名
     * @param fromConfigFile 是否来源于配置文件
     */
    String getPackage(String poPackage, boolean fromConfigFile);


    /**
     * Created By zby on 17:52 2019/4/1
     * 处理子文件的部分路径
     *
     * @param poPackage      包名，如果不传，默认是jdbc.package名字
     * @param fromConfigFile 是否来源于配置文件
     * @param filePath       子文件路径
     */
    String packageChildPath(String poPackage, boolean fromConfigFile, String filePath);
}
