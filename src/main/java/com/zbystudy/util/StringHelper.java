package com.zbystudy.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created By zby on 10:47 2019/4/3
 * <p>
 * 判断字符串中是否包含某个字符串的值，如果包含，则返回其起始和结束位置
 */

public class StringHelper {

    /**
     * 包含字符串的起始下标位置
     */
    private static int startIndex = 0;

    /**
     * 包含字符串的结束字符长度
     */
    private static int endLength = 0;

    /**
     * Created By zby on 10:55 2019/4/3
     *
     * @param srcLetter 源字符串
     * @param strToFind 查找字符串
     */
    public static boolean contains(String srcLetter, String strToFind) {
//        包含字符串
        if (StringUtils.isAllBlank(srcLetter, strToFind)) {
            return false;
        }
        char[] srcChars = srcLetter.toCharArray();
        for (int i = 0; i < srcChars.length; i++) {
            char[] charToFinds = strToFind.toCharArray();
            if (srcChars[i] == charToFinds[0]) {
                for (int j = 0; j < charToFinds.length; j++) {
                    if (srcChars[i + j] != charToFinds[j]) {
                        break;
                    }
                }
//               如果包含字符串，则返回
                startIndex = i;
                endLength = i + charToFinds.length;
                return true;
            }
        }
        return false;
    }

    /**
     * Created By zby on 11:17 2019/4/3
     *
     * @return 字符串所在的末位置
     */
    public static int getStartIndex(String srcLetter, String strToFind) {
        contains(srcLetter, strToFind);
        return startIndex;
    }

    /**
     * Created By zby on 11:23 2019/4/3
     *
     * @return 字符串所在的末位置
     */
    public static int getEndLength(String srcLetter, String strToFind) {
        contains(srcLetter, strToFind);
        return endLength;
    }

}
