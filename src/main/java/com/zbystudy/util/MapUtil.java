package com.zbystudy.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Created By zby on 13:35 2019/4/8
 */

public class MapUtil {

    /**
     * Created By zby on 13:36 2019/4/8
     * 判断map中是否有重名的key值
     */
    public static boolean existKey(String tableName, Map<String, StringBuilder> tableMap) {
        boolean paramCheck = (null != tableMap && !tableMap.isEmpty()) && isNotBlank(tableName);
        if (paramCheck) {
            for (Map.Entry<String, StringBuilder> entry : tableMap.entrySet()) {
                if (tableName.equals(entry.getKey())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Created By zby on 0:09 2019/4/9
     *
     * @param src 原始容器
     */
    public static Map<String, StringBuilder> reverseMap(Map<String, StringBuilder> src) {
        Map<String, StringBuilder> reverseMap = null;
        if (src != null || src.size() > 0) {
            List<String> keySets = new ArrayList<>(src.keySet());
            reverseMap = new LinkedHashMap<>();
            for (int i = keySets.size() - 1; i >= 0; i--) {
                String key = keySets.get(i);
                reverseMap.put(key, src.get(key));
            }
        }
        return reverseMap;
    }

    /**
     * Created By zby on 10:41 2019/4/9
     * 判断key Null
     */
    public static boolean isKeyBlank(Map<String, StringBuilder> src) {
        if (src != null || src.size() > 0) {
            for (Map.Entry<String, StringBuilder> entry : src.entrySet()) {
                if (isBlank(entry.getKey())) {
                    return true;
                }
            }
        }
        return false;
    }

}
