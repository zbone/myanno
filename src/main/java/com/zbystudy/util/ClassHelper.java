package com.zbystudy.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created By zby on 14:28 2019/4/3
 */
public class ClassHelper {

    /**
     * Created By zby on 22:44 2019/3/2
     * 类型转化为属性名
     *
     * @param className 类名
     */
    public static String classNameToProName(String className) {
        if (StringUtils.isBlank(className)) {
            throw new RuntimeException("类型不能为空className=" + className);
        }
        return StringUtils.substring(className, 0, 1).toLowerCase() + StringUtils.substring(className, 1);
    }
}
