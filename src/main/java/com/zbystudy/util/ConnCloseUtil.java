package com.zbystudy.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created By zby on 9:23 2019/4/8
 */

public class ConnCloseUtil {

    /**
     * Created By zby on 9:23 2019/4/8
     * 关闭连接
     */
    public static void closeConn(Connection conn) {
        if (null != conn) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Created By zby on 21:09 2019/4/8
     * 关闭连接和预处理器
     */
    public static void closeState(Statement state) {
        if (null != state) {
            try {
                state.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Created By zby on 9:26 2019/4/8
     * 关闭连接和预处理器
     */
    public static void closeConn(Connection conn, Statement state) {
        closeConn(conn);
        closeState(state);
    }

    /**
     * Created By zby on 9:28 2019/4/8
     * 关闭连接和预处理器、查询的数据集
     */
    public static void closeConn(Connection conn, Statement state, ResultSet rs) {
        closeConn(conn, state);
        if (null != rs) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
