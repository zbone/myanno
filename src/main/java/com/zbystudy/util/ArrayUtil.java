package com.zbystudy.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created By zby on 14:58 2019/4/2
 * <p>
 * 数组操作工具类
 */

public class ArrayUtil {

    /**
     * Created By zby on 20:50 2019/2/13
     * 判断字符串是否为null
     */
    public static boolean isEmpty(Object[] objects) {
        if (null == objects || objects.length == 0)
            return true;
        for (Object object : objects) {
            if (null != object)
                return false;
        }
        return true;
    }


    /**
     * Created By zby on 20:50 2019/2/13
     * 判断字符串是否为null
     */
    public static boolean isNotEmpty(Object[] objects) {
        if (null == objects && objects.length == 0)
            return false;
        for (Object object : objects) {
            if (null == object)
                return false;
        }
        return true;
    }

    /**
     * Created By zby on 15:00 2019/2/14
     * 判断参数是否在该数组中
     *
     * @param param   参数
     * @param filters 数组
     */
    public static boolean containsAny(String param, String[] filters) {
        if (isNotEmpty(filters)) {
            if (StringUtils.isNotBlank(param)) {
                for (String tmp : filters) {
                    if (tmp.equals(param)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Created By zby on 22:10 2019/2/20
     * 判断数组
     */
    public static void validateArray(Class clazz, Object[] objects) {
        if (clazz == null) {
            throw new RuntimeException("类不存在");
        }
        if (null == objects) {
            throw new RuntimeException("类" + clazz.getName() + "无构造器");
        }
        if (objects.length > 1) {
            throw new RuntimeException("如果是父类，仅需给父类一个构造器");
        }
    }


}
