package com.zbystudy.util;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Created By zby on 21:52 2019/4/8
 * 处理sql的工具类
 */

public class SqlUtil {

    public static String transNoAction(String noAction) {
        return isNotBlank(noAction) && noAction.equals("NO_ACTION") ? "NO ACTION" : noAction;
    }
}
