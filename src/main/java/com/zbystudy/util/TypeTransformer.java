package com.zbystudy.util;

import com.zbystudy.constants.JavaBasicTypes;
import com.zbystudy.constants.SqlBasicTypes;

/**
 * Created By zby on 15:39 2019/3/31
 * 数据类型转化,，将Java的属性数据类型转化为sql字段的数据类型
 */

public class TypeTransformer {


    /**
     * Created By zby on 15:44 2019/3/31
     *
     * @param typeName 类型名称
     */
    public static String javaToSql(String typeName) {
        if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_INT)) {
            return SqlBasicTypes.SQL_TYPE_INT + "(11)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_INTEGER)) {
            return SqlBasicTypes.SQL_TYPE_INTEGER + "(11)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_LONG)) {
            return SqlBasicTypes.SQL_TYPE_BIG_INTEGER + "(20)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_SHORT)) {
            return SqlBasicTypes.SQL_TYPE_SMALLINT + "(6)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_FLOAT)) {
            return SqlBasicTypes.SQL_TYPE_FLOAT;
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_DOUBLE)) {
            return SqlBasicTypes.SQL_TYPE_DOUBLE;
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_BIG_DECIMAL)) {
            return SqlBasicTypes.SQL_TYPE_NUMERIC + "(10)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_STRING)) {
            return SqlBasicTypes.SQL_TYPE_VARCHAR + "(255)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_BYTE)) {
            return SqlBasicTypes.SQL_TYPE_TINYINT + "(4)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_BOOLEAN)) {
            return SqlBasicTypes.SQL_TYPE_BIT + "(1)";
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_DATE)) {
            return SqlBasicTypes.SQL_TYPE_DATE;
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_TIME)) {
            return SqlBasicTypes.SQL_TYPE_TIME;
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_TIMESTAMP)
                || typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_CALENDAR)) {
            return SqlBasicTypes.SQL_TYPE_TIMESTAMP;
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_Clob)) {
            return SqlBasicTypes.SQL_TYPE_LONGTEXT;
        } else if (typeName.equalsIgnoreCase(JavaBasicTypes.JAVA_TYPE_Blob)) {
            return SqlBasicTypes.SQL_TYPE_BLOB;
        } else {
            return null;
        }
    }

}
