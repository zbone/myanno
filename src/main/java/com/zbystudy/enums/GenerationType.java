package com.zbystudy.enums;

/**
 * Created By zby on 14:28 2019/3/31
 * 主键增长类型
 */
public enum GenerationType {
    /**
     *
     */
    TABLE,

    SEQUENCE,

    IDENTITY,

    AUTO;
}
