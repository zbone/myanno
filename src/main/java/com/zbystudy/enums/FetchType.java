package com.zbystudy.enums;

/**
 * Created By zby on 23:07 2019/3/31
 */
public enum FetchType {

    /** Defines that data can be lazily fetched. */
    LAZY,

    /** Defines that data must be eagerly fetched. */
    EAGER
}
