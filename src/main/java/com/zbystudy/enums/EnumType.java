package com.zbystudy.enums;

/**
 * Created By zby on 13:59 2019/3/31
 * <p>
 * 这是操作枚举类型的属性，枚举分为顺序值，即ordinal
 * 也分为key值，比如PERSON_TYPE_SALESMAN
 */
public enum EnumType {

    /**
     * 这是枚举的顺序之，比如枚举ORDINAL的值为1
     */
    ORDINAL,

    /**
     * 这是枚举的key值，比如当前枚举的ORDINAL、STRING
     */
    STRING;
}
