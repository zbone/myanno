package com.zbystudy.enums;

/**
 * Created By zby on 23:04 2019/3/31
 * 级联方式
 */
public enum CascadeType {

    CASCADE,

    RESTRICT,

    NO_ACTION,

    SET_NULL;
}
