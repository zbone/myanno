package com.zbystudy.po;

import com.zbystudy.anno.*;
import com.zbystudy.enums.GenerationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created By zby on 15:51 2019/3/31
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tb_student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, name = "id", columnDefinition = "BIGINT(11) comment '主键'")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "sex", columnDefinition = "TINYINT(3) COMMENT '性别'", nullable = true)
    private Byte sex;
}
