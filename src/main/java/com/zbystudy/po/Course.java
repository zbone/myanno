package com.zbystudy.po;

import com.zbystudy.anno.*;
import com.zbystudy.enums.CascadeType;
import com.zbystudy.enums.EnumType;
import com.zbystudy.enums.GenerationType;
import com.zbystudy.poenums.SubjectTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Field;

/**
 * Created By zby on 15:51 2019/3/31
 * 学生课程表
 */
@Entity
@Table(name = "tb_course")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, name = "id", columnDefinition = " comment '主键'")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "subject_type", columnDefinition = "COMMENT '课程名称，存储枚举key值'")
    private SubjectTypeEnum subjectType;


    @Column(name = "teacher_name", columnDefinition = "COMMENT '老师名称'")
    private String teacherName;


    /**
     * 0 表示删除关联，1 表示更新关联
     */
    @ManyToOne(cascade = {CascadeType.CASCADE, CascadeType.NO_ACTION})
    @JoinColumn(name = "student_id", columnDefinition = "BIGINT(20) COMMENT '外键是学生表'", table = "tb_student", nullable = false)
    private Student student;

    @Column(name = "is_deleted")
    private boolean deleted = false;

    public static void main(String[] args) {
//        通过反射修改final修饰的属性值，但只能修改包装类的属性值
        Class clazz = Course.class;
        try {
            Course course = (Course) clazz.newInstance();
            Field field = clazz.getDeclaredField("id");
            field.setAccessible(true);
            field.set(course, 6L);
            System.out.println(course.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
