package com.zbystudy.po;

import com.zbystudy.anno.*;
import com.zbystudy.enums.GenerationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created By zby on 21:21 2019/4/2
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tb_score")
public class Score {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, name = "id", columnDefinition = " comment '主键'")
    private Long id;

    @Column(name = "score")
    private Double score;

    /**
     * 0 表示删除关联，1 表示更新关联
     */
    @ManyToOne
    @JoinColumn(name = "course_id", columnDefinition = "BIGINT(18) COMMENT '外键是课程表'")
    private Course course;


}
