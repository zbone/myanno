package com.zbystudy.builder;

import com.zbystudy.core.vo.ForeignTable;

/**
 * Created By zby on 13:56 2019/4/3
 */

public class ForeignTableBuilder {

    /**
     * Created By zby on 13:58 2019/4/3
     * 创建外键表信息
     */
    public static ForeignTable instance() {
        return new ForeignTable();
    }
}
