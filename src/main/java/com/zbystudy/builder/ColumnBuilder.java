package com.zbystudy.builder;

import com.zbystudy.core.TableColumn;

/**
 * Created By zby on 15:36 2019/4/3
 * 创建表
 */

public class ColumnBuilder {

    /**
     * Created By zby on 15:38 2019/4/3
     * 创建主表信息
     */
    public static TableColumn instance() {
        return new TableColumn();
    }
}
