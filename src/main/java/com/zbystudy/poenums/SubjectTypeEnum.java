package com.zbystudy.poenums;

/**
 * Created By zby on 23:34 2019/4/1
 * 课程枚举
 */
public enum SubjectTypeEnum implements TitleEnum {

    SUBJECT_TYPE_CHINESE("语文"),
    SUBJECT_TYPE_MATH("数学"),
    SUBJECT_TYPE_ENGLISH("英语");

    public String title;

    SubjectTypeEnum(String title) {
        this.title = title;
    }

    @Override
    public String getTitle() {
        return title;
    }
}
