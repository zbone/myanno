package com.zbystudy.poenums;

/**
 * Created By zby on 23:36 2019/4/1
 */
public interface TitleEnum {

    String getTitle();
}
