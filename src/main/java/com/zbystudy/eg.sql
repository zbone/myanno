-- 如果学生表存在，即删除学生表
DROP TABLE if EXISTS tb_student ;

-- 创建学生表
CREATE TABLE `tb_student` (
	`id` INT (11) NOT NULL AUTO_INCREMENT COMMENT '主键' PRIMARY KEY,
	`name` VARCHAR (255) COMMENT '姓名' NULL,
	`sex` bit (1) COMMENT '性别' NULL
) ENGINE = INNODB DEFAULT CHARACTER SET = utf8;

-- 如果课程表存在，则删除课程表
DROP TABLE if EXISTS tb_course ;

-- 创建课程表
CREATE TABLE `tb_course` (
	`id`  int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键' PRIMARY KEY,
	`score`  double NULL DEFAULT 0 COMMENT '分数' ,
	`subject_name`  enum('SUBJECT_TYPE_CHINESE','SUBJECT_TYPE_MATH','SUBJECT_TYPE_ENGLISH') NULL  COMMENT '课程名称，存储枚举key值' ,
	`teacher_name`  varchar(255)  NULL DEFAULT NULL COMMENT '老师名称' ,
	`student_id`  int(11) NOT NULL,
	CONSTRAINT fk_student_course  FOREIGN KEY (`student_id`) REFERENCES `tb_student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

-- 如果分数表存在，则删除分数表
DROP TABLE if EXISTS tb_score ;

-- 创建分数表
CREATE TABLE `tb_score`(
	id BIGINT(20) not null AUTO_INCREMENT  comment '主键' primary key,
	score DOUBLE null  ,
	course_id BIGINT(18) null  COMMENT '外键是课程表' ,
	CONSTRAINT fk_course_score FOREIGN KEY(`course_id`) REFERENCES `tb_course`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8

