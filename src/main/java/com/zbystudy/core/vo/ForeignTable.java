package com.zbystudy.core.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created By zby on 13:49 2019/4/3
 * 当前表关联的外键表不止一个，因而是一个集合
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ForeignTable {

    /**
     * 级联关系
     */
    private String cascade;

    /**
     * 外键名字
     */
    private String foreignKeyName;

    /**
     * 外键表名
     */
    private String foreignTableName;

    /**
     * 外键表主键
     */
    private String foreignTablePk;

    /**
     * 外键名称
     */
    private String foreignName;


}
