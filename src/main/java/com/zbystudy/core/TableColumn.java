package com.zbystudy.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created By zby on 15:33 2019/4/2
 * 记录字段信息
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TableColumn {

    /**
     * 当前表名
     */
    private String tableName;

    /**
     * 字段名称
     */
    private String colName;

    /**
     * 字段长度
     */
    private int colLength = 0;

    /**
     * 字段定义
     */
    private String colDefinition;

    /**
     * 字段是否空值，比如 null not null
     */
    private String colNull;

    /**
     * 字段唯一性 unique
     */
    private String colUnique;

    /**
     * 主键标志
     */
    private String colPk;

    /**
     * 主键字段
     */
    private String colPkVal;

    /**
     * 增长策略
     */
    private String incrementStrategy;

    /**
     * 字段类型
     */
    private String colType;

    /**
     * 级联关系
     */
    private String colCascade;

    /**
     * 外键表名
     */
    private String foreignTable;


}
