package com.zbystudy.anno;

import com.zbystudy.enums.CascadeType;
import com.zbystudy.enums.FetchType;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static com.zbystudy.enums.FetchType.EAGER;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 23:02 2019/3/31
 */
@Target({FIELD, METHOD})
@Retention(RUNTIME)
public @interface ManyToOne {


    /**
     * 级联方式
     */
    CascadeType[] cascade() default {};

    /**
     * 加载方式
     */
    FetchType fetch() default EAGER;


    boolean optional() default true;
}
