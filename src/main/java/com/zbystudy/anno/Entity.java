package com.zbystudy.anno;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 13:40 2019/3/31
 * 这是entity注解
 */
//这是将注解应用到的Javadoc文档中
@Documented
//这是是否继承父类的注解
//@Inherited
//将注解应用到哪里，类注解，属性注解，方法注解
@Target(TYPE)
//表示在什么级别保存该注解信息
@Retention(RUNTIME)
public @interface Entity {

    String name() default "";
}
