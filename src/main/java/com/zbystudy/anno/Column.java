package com.zbystudy.anno;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 14:07 2019/3/31
 * 表名类型的信息
 */
@Target({FIELD,METHOD})
@Retention(RUNTIME)
public @interface Column {

    /**
     * 字段名
     */
    String name() default "";

    /**
     * 字符长度
     */
    int length() default 255;

    /**
     * 字段是否为空
     */
    boolean nullable() default true;

    /**
     * 是否是唯一值
     */
    boolean unique() default false;

    /**
     * 字段类型定义
     */
    String columnDefinition() default "";


}
