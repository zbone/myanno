package com.zbystudy.anno;

import com.zbystudy.enums.EnumType;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static com.zbystudy.enums.EnumType.ORDINAL;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 13:54 2019/3/31
 * 处理枚举类型的注解
 */
@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface Enumerated {

    EnumType value() default ORDINAL;
}
