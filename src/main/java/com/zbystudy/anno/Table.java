package com.zbystudy.anno;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 13:45 2019/3/31
 * 表名注解
 */
@Target(TYPE)
@Retention(RUNTIME)
public @interface Table {

    /**
     * 表名
     */
    String name() default "";

    /**
     * 表属于哪个数据库
     */
    String catelog() default "";


}
