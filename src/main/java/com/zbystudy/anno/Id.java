package com.zbystudy.anno;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 23:17 2019/4/1
 * 自增主键
 */
@Target({METHOD,FIELD})
@Retention(RUNTIME)
public @interface Id {
}
