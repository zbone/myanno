package com.zbystudy.anno;

import com.zbystudy.enums.GenerationType;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static com.zbystudy.enums.GenerationType.AUTO;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 14:26 2019/3/31
 */
@Target({FIELD, METHOD})
@Retention(RUNTIME)
public @interface GeneratedValue {

    /**
     * 主键生成策略
     */
    GenerationType strategy() default AUTO;

    /**
     * 主键生成使用规定
     */
    String generator() default "";
}
