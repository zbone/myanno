package com.zbystudy.anno;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created By zby on 23:08 2019/3/31
 * <p>
 * 外键字段名
 */
@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface JoinColumn {
    /**
     * 外键名
     */
    String name() default "";


    String referencedColumnName() default "";


    boolean unique() default false;

    /**
     * 外键值是否为空
     */
    boolean nullable() default true;

    String columnDefinition() default "";

    String table() default "";
}
