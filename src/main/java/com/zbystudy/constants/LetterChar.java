package com.zbystudy.constants;

/**
 * Created By zby on 22:21 2019/4/8
 * <p>
 * 字符串
 */

public class LetterChar {

    /**
     * 换行符
     */
    public static final String LINE_FEED_OP = "\n";

    /**
     * 串联符
     */
    public static final String SERIES_COMMA_OP = ",";


    /**
     * 示意符
     */
    public static final String SIGN_COLON_OP = ":";

    /**
     * 连接符
     */
    public static final String CONNECT_JOIN_OP = "&";

    /**
     * 赋值符
     */
    public static final String ASSIGN_OP = "=";

    /**
     * 问号符
     */
    public static final String QUES_OP = "?";

    /**
     * tab符
     */
    public static final String TAB_OP = "\t";

    /**
     * 空格符
     */
    public static final String BLANK_OP = " ";

    /**
     * 空字符
     */
    public static final String NUL_OP = "";

    /**
     * 左括号
     */
    public static final String LEFT_BRACKET = "(";

    /**
     * 右括号
     */
    public static final String RIGHT_BRACKET = ")";

    /**
     * 反单引号
     */
    public static final String BACK_QUOTE = "`";

    /**
     * 反单引号
     */
    public static final String SINGLE_QUOTES = "'";

    /**
     * 下划线
     */
    public static final String UNDERLINE = "_";

    /**
     * null
     */
    public static final String NULL = "null";

    /**
     * null
     */
    public static final String NOT_NULL = "not null";

}
