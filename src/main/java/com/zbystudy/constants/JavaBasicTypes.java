package com.zbystudy.constants;

/**
 * Created By zby on 15:45 2019/3/31
 * Java 数据类型类型
 *
 * @author zby
 */

public class JavaBasicTypes {

    public static final String JAVA_TYPE_INTEGER = "Integer";

    public static final String JAVA_TYPE_LONG = "Long";

    public static final String JAVA_TYPE_SHORT = "Short";

    public static final String JAVA_TYPE_FLOAT = "float";

    public static final String JAVA_TYPE_DOUBLE = "double";

    public static final String JAVA_TYPE_BIG_DECIMAL = "BigDecimal";

    public static final String JAVA_TYPE_STRING = "String";

    public static final String JAVA_TYPE_BYTE = "Byte";

    public static final String JAVA_TYPE_BOOLEAN = "Boolean";

    public static final String JAVA_TYPE_INT = "int";

    public static final String JAVA_TYPE_DATE = "Date";

    public static final String JAVA_TYPE_TIME = "Time";

    public static final String JAVA_TYPE_TIMESTAMP = "Timestamp";

    public static final String JAVA_TYPE_CALENDAR = "Calendar";

    public static final String JAVA_TYPE_Clob = "Clob";

    public static final String JAVA_TYPE_Blob = "Blob";
}
