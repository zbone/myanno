package com.zbystudy.constants;

/**
 * Created By zby on 17:22 2019/4/2
 * SQL数据库的类型
 *
 * @author zby
 */

public class SqlBasicTypes {

    public static final String SQL_TYPE_INT = "INT";

    public static final String SQL_TYPE_INTEGER = "INTEGER";

    public static final String SQL_TYPE_BIG_INTEGER = "BIGINT";

    public static final String SQL_TYPE_SMALLINT = "SMALLINT";

    public static final String SQL_TYPE_FLOAT = "FLOAT";

    public static final String SQL_TYPE_DOUBLE = "DOUBLE";

    public static final String SQL_TYPE_NUMERIC = "NUMERIC";

    public static final String SQL_TYPE_CHAR = "CHAR";

    public static final String SQL_TYPE_VARCHAR = "VARCHAR";

    public static final String SQL_TYPE_TINYINT = "TINYINT";

    public static final String SQL_TYPE_BIT = "BIT";

    public static final String SQL_TYPE_DATE = "DATE";

    public static final String SQL_TYPE_TIME = "TIME";

    public static final String SQL_TYPE_TIMESTAMP = "TIMESTAMP";

    public static final String SQL_TYPE_LONGTEXT = "LONGTEXT";

    public static final String SQL_TYPE_BLOB = "BLOB";


}
