package com.zbystudy;

import com.zbystudy.factory.PackageEntityFactory;
import org.junit.Ignore;

import java.util.List;

/**
 * Created By zby on 22:17 2019/4/1
 * 测试
 */

public class EntityFactoryTest {

    @Ignore
    public void test() {
        List<String> entityFileNames = PackageEntityFactory.getInstance()
                .loadEntityFileNames("jdbc.package", true);
        for (String entityFileName : entityFileNames) {
            System.out.println(entityFileName);
        }
    }
}
