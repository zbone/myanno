package com.zbystudy;

import com.zbystudy.core.TableInit;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By zby on 16:10 2019/4/3
 */

public class CreationTableLoaderTest {

    @Test
    public void test() {
        Map<String, StringBuilder> stringBuilders = TableInit.getDbTableMap();
//        for (Map.Entry<String, StringBuilder> entry : stringBuilders.entrySet()) {
//            System.out.println(entry.getKey() + "\n\t" + entry.getValue());
//        }
        List<String> keySets = new ArrayList<>(stringBuilders.keySet());
        Map<String, StringBuilder> reverseMap = new LinkedHashMap<>();
        for (int i = keySets.size() - 1; i >= 0; i--) {
            String key = keySets.get(i);
            reverseMap.put(key, stringBuilders.get(key));
        }
        for (Map.Entry<String, StringBuilder> entry : reverseMap.entrySet()) {
            System.out.println(entry.getKey() + "\n\t" + entry.getValue());
        }
    }
}
