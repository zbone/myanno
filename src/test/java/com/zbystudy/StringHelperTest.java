import org.junit.Ignore;

import static com.zbystudy.util.StringHelper.*;

/**
 * Created By zby on 11:28 2019/4/3
 * 测试字符串类型
 */

public class StringHelperTest {

    @Ignore
    public void testcontainsAny() {
        String srcLetter = "BIGINT(11) comment '主键'";
        String strToFind = "INT";
        System.out.println(contains(srcLetter, strToFind));
    }

    @Ignore
    public void testStartEnd() {
        String srcLetter = "ssaabbaa";
        String strToFind = "a";
        System.out.println("startIndex=" + getStartIndex(srcLetter,strToFind) + ",endLength=" + getEndLength(srcLetter,strToFind));
    }

}
